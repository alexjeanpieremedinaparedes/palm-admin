const { guessProductionMode } = require("@ngneat/tailwind");

process.env.TAILWIND_MODE = guessProductionMode() ? 'build' : 'watch';

module.exports = {
    prefix: '',
    mode: 'jit',
    purge: {
      content: [
        './src/**/*.{html,ts,css,scss,sass,less,styl}',
      ]
    },
    darkMode: 'class', // or 'media' or 'class'
    theme: {
      extend: {

        fontWeight: {
          hairline: 100,
          'extra-light': 100,
          thin: 200,
          light: 300,
          normal: 400,
          medium: 500,
          grande: 580,
          semibold: 600,
          masgrande: 605,
          bold: 700,
          extrabold: 800,
          'extra-bold': 800,
          black: 900,
      },

      width: {
        '1/7': '14.2857143%',
        '2/7': '28.5714286%',
        '3/7': '42.8571429%',
        '4/7': '57.1428571%',
        '5/7': '71.4285714%',
        '6/7': '85.7142857%',
        '33': '11rem',
        '40': '17.5rem',
        '83': '22rem',
        '97': '21rem',
        '98': '29rem',
        '99': '34.875rem',
        '99.4': '40.625',
        '100': '41.813rem',
        '110': '42rem',
        '120': '45rem',
        '130': '48rem',
        '140': '50rem'
    },

    height: {
        '82': '33rem',
        '95': '35rem',
        '100':'39rem',
        '110':'42rem'

    },


        colors: {

          'morado':{
            '150':'#FCF4FF',
            '500':'#6E40D6',
            '550':'#8380F9',

          },


          'plomo': {
            '100':'#FBFBFB',
            '190':'#F5F6FA',
            '200':'#C4C4C4',
            '210':'#B9B9B9',
            '250':'#E7E7E7'
          },

          'azul':{
            '300':'#13385D',
            '500':'#242424'
          },

          'verde':{
            '500': '#2FC974',
          },

          'naranja':{
            '500': '#FF450A',
          },
          'rojo':{
            '500':'#EB0C34'
          }



      },

        fontFamily: {
          Roboto: ['Roboto'],
          Poppins: ['Poppins'],
          Manrope: ['Manrope']
      }



      },
    },
    variants: {
      extend: {},
    },
    plugins: [require('@tailwindcss/aspect-ratio'),require('@tailwindcss/forms'),require('@tailwindcss/line-clamp'),require('@tailwindcss/typography')],
};
