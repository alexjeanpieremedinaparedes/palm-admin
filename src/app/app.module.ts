import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material-module';
import { AuthModule } from './auth/auth.module';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './layouts/dashboard/dashboard.component';
import { AuthComponent } from './layouts/auth/auth.component';
import { SharedModule } from './shared/shared.module';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { registerLocaleData } from '@angular/common';
import LocaleEsPe from '@angular/common/locales/es-PE.js';
import { HttpClientModule } from '@angular/common/http';
registerLocaleData(LocaleEsPe);



@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AuthComponent,
 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    AuthModule,
    SharedModule
  ],
  providers: [
    { 
      provide: MAT_DATE_LOCALE,
      useValue: 'es-PE'
    },
    {
      provide: LOCALE_ID,
      useValue: 'es-PE'
    },
    // { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
