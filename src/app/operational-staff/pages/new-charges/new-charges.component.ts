import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { ValidationsService } from 'src/app/core/service/validations.service';
import { IChargesResult } from '../../interface/charges.interface';
import { EmployeeService } from '../../service/employee.service';

@Component({
  selector: 'app-new-charges',
  templateUrl: './new-charges.component.html',
  styleUrls: ['./new-charges.component.css']
})
export class NewChargesComponent implements OnInit {

  public form!: FormGroup;
  constructor(
    public sfn: FunctionsService,
    private fb: FormBuilder,
    private sval: ValidationsService,
    private semployee: EmployeeService,
    private spinner: NgxSpinnerService,
    private close: MatDialogRef<null>,
    @Optional() @Inject( MAT_DIALOG_DATA ) public body: any,
  ) {
    this.createForm();
    if(body.edit) this.editForm();
  }

  ngOnInit(): void {
  }

  public get chargeInvalid() {
    return this.sval.ctrInvalid('cargoempleado', this.form);
  }

  private createForm() {
    this.form = this.fb.group({
      idcargoempleado: [ 0 ],
      cargoempleado: [ '', [ Validators.required ] ],
      editar: [ false ]
    });
  }

  private editForm() {
    const item: IChargesResult = this.body.item;
    this.form.patchValue({
      idcargoempleado: item.idcargoempleado,
      cargoempleado: item.cargoempleado,
      editar: true
    });
  }

  public saveCharge() {
    if(this.form.invalid) {
      return this.sval.emptyData(this.form);
    }

    this.spinner.show();
    const body = { ... this.form.value };
    this.semployee.saveCharge(body).subscribe(response =>{

      const ok = response.status === 200;
      if(ok) {
        const status = this.body.edit ? 'editado' : 'agregado';
        this.sfn.openSnackBar(`Perfil ${status} con exito`);
        this.close.close({ ok: true });
      }

      this.spinner.hide();
    }, (e) =>{
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

}
