import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ITypeDocumentResult } from 'src/app/core/interface/attribute.interface';
import { AttributeService } from 'src/app/core/service/attribute.service';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { ValidationsService } from 'src/app/core/service/validations.service';
import { IChargesResult, IEmployeeResult } from '../../interface/charges.interface';
import { NewChargesComponent } from '../new-charges/new-charges.component';
import { EmployeeService } from '../../service/employee.service';

@Component({
  selector: 'app-new-operational-staff',
  templateUrl: './new-operational-staff.component.html',
  styleUrls: ['./new-operational-staff.component.css']
})
export class NewOperationalStaffComponent implements OnInit {

  @Input() body: any;
  @Output() closeForm  = new EventEmitter<boolean>();
  @Output() responseok  = new EventEmitter<boolean>();

  public listCharges: IChargesResult[] = [];
  public listTypeDocument: ITypeDocumentResult[] = [];

  form!: FormGroup;
  
  constructor(
    public sfn: FunctionsService,
    private modals:MatDialog,
    private fb: FormBuilder,
    private sval: ValidationsService,
    private semployee: EmployeeService,
    private sattribute: AttributeService,
    private spinner: NgxSpinnerService
    ) {
      this.getCharges();
      this.getTypeDocument();
      this.createForm();
    }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {

    const edit = changes['body'].currentValue.edit;
    const item: IEmployeeResult = changes['body'].currentValue.item;
    if(edit) {
      this.editForm(item);
    }
  }

  public get idtipodocumentoInvalid() {
    return this.sval.ctrInvalid('idtipodocumento', this.form);
  }

  public get idcargoempleadoInvalid() {
    return this.sval.ctrInvalid('idcargoempleado', this.form);
  }

  public get numerodocumentoInvalid() {
    return this.sval.ctrInvalid('numerodocumento', this.form);
  }

  public get nombreInvalid() {
    return this.sval.ctrInvalid('nombre', this.form);
  }

  public get apellidosInvalid() {
    return this.sval.ctrInvalid('apellidos', this.form);
  }

  public get fechanacimientoInvalid() {
    return this.sval.ctrInvalid('fechanacimiento', this.form);
  }
  
  public get telefonoInvalid() {
    return this.sval.ctrInvalid('telefono', this.form);
  }

  public get correoInvalid() {
    return this.sval.ctrInvalid('correo', this.form);
  }

  public createForm() {
    this.form = this.fb.group({
      idempleado      : [ 0 ],
      idtipodocumento : [ '', [ Validators.required ] ],
      idcargoempleado : [ '', [ Validators.required ] ],
      numerodocumento : [ '', [ Validators.required ] ],
      nombre          : [ '', [ Validators.required ] ],
      apellidos       : [ '', [ Validators.required ] ],
      fechanacimiento : [ '', [ Validators.required ] ],
      direccion       : [ '' ],
      telefono        : [ '', [ Validators.minLength(6), Validators.maxLength(9) ] ],
      correo          : [ '', [ Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$') ] ],
      sexo            : [ true ],
      editar          : [ false ]
    });
  }

  public editForm(item: IEmployeeResult) {
    this.form.patchValue({
      idempleado      : item.idempleado,
      idtipodocumento : item.idtipodocumento,
      idcargoempleado : item.idcargo,
      numerodocumento : item.numerodocumento,
      nombre          : item.nombre,
      apellidos       : item.apellidos,
      fechanacimiento : this.sfn.dateMoment(item.fechanacimiento),
      direccion       : item.direcion,
      telefono        : item.telefono,
      correo          : item.correo,
      sexo            : item.sexo,
      editar          : true
    });
  }
  
  public closeFrm() {
    this.closeForm.emit(false);
  }

  public newCharges(){
    
    const body = {
      edit: false,
      item: null
    };

    this.modals.open(NewChargesComponent, {
      data: body,
      disableClose: true
    }).afterClosed().subscribe( result => {
      if( result.ok ) {
        this.getCharges();
      }
    })
  }

  public getCharges() {

    this.spinner.show();
    this.semployee.getCharge().subscribe( response => {

      this.listCharges = response;
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  getTypeDocument() {

    this.spinner.show();
    this.sattribute.gettypeDocument().subscribe( response => {

      this.listTypeDocument = response;
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  saveEmployee() {

    if(this.form.invalid) {
      return this.sval.emptyData(this.form);
    }

    this.spinner.show();
    const body = { ... this.form.value };
    this.semployee.saveEmployee(body).subscribe( response =>{

      const ok = response.status === 200;
      if(ok) {
        const status = this.body.edit ? 'editado' : 'agregado';
        this.sfn.openSnackBar(`Personal Operativo ${status} con exito`);
        this.responseok.emit(true);
        this.closeFrm();
      }
      this.spinner.hide();
    }, (e)=>{
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  validationDocs($event: any): boolean {
    const idtypeDoc = this.form.value.idtipodocumento;
    const docs = this.listTypeDocument.find(x=>x.idtipodocumento === idtypeDoc);
    if(!docs) return false ;
    const type = String(docs.tipodocumento).toUpperCase();
    const leng = type === 'DNI' ? 8 : type === ('PASAPORTE' || 'CARNET EXT') ? 12 : 15;
    return this.sfn.onlyNumberAllowed($event, leng)
  }

}
