import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { SettingService } from 'src/app/core/service/setting.service';
import { IEmployeeResult } from '../../interface/charges.interface';
import { EmployeeService } from '../../service/employee.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-operational-staff',
  templateUrl: './list-operational-staff.component.html',
  styleUrls: ['./list-operational-staff.component.css']
})
export class ListOperationalStaffComponent implements AfterViewInit, OnInit {
  
  public body: any;

  public newOperationaStaff  :  boolean  =  false;
  public openlistCharge      :  boolean  =  false;
  public showPaginator       :  boolean  =  false;
  public displayedColumns    : string[]  =  ['Cargo', 'ApellidosNombres', 'Direccion', 'Celular', 'Correo', 'opciones' ];
  public dataSource = new MatTableDataSource<IEmployeeResult>([]);
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  
  constructor(
    private modals: MatDialog,
    private spinner: NgxSpinnerService,
    private sfn: FunctionsService,
    private semployee: EmployeeService,
    private ssetting: SettingService,
    private router: Router
  ) {
    this.getEmployee();
  }

  ngOnInit(): void {

    this.ssetting.textSearch$.subscribe( response => {
      const urlActual = this.router.url;
      const urlReceived = response.url;
      if( urlActual === urlReceived ) {
        this.dataSource.filter = response.value;
      }
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  public operationalStaff(edit: boolean = false, item: any = null) {
    this.newOperationaStaff = true;
    this.body = {
      edit: edit,
      item: item
    };
  }

  public openListCharges() {
    this.openlistCharge = true;
  }

  public async deleteEmployee( item: IEmployeeResult ) {
    const head1 = 'Desea';
    const head2 = 'eliminar';
    const head3 = 'el personal operativo';
    const message = 'Se eliminara el personal operativo';
    const contine = await this.sfn.modalQuestion(head1, head2, head3, message, true);
    if(contine) {
      
      this.spinner.show();
      const id = item.idempleado;
      this.semployee.deleteEmployee(id).subscribe( response => {

        const ok = response.status === 200;
        if(ok) {
          this.sfn.openSnackBar('Personal operativo eliminado con exito');
          this.getEmployee();
        }
        this.spinner.hide();
      }, (e) => {
        this.spinner.hide();
        this.sfn.except(e);
      });
    }
  }

  public getEmployee() {

    this.spinner.show();
    this.semployee.getEmployee().subscribe( response => {

      this.dataSource = new MatTableDataSource<IEmployeeResult>(response);
      this.dataSource.paginator = this.paginator;
      this.showPaginator = this.dataSource.data.length > 10;
      if(this.showPaginator) this.paginator._intl.itemsPerPageLabel = 'items por pagina';
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

}
