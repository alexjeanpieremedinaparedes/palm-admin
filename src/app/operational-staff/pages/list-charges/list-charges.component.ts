
import {  
  AfterViewInit,
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { NewChargesComponent } from '../new-charges/new-charges.component';
import { IChargesResult } from '../../interface/charges.interface';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { EmployeeService } from '../../service/employee.service';

@Component({
  selector: 'app-list-charges',
  templateUrl: './list-charges.component.html',
  styleUrls: ['./list-charges.component.css']
})
export class ListChargesComponent implements AfterViewInit, OnInit {

  @Output() closeForm = new EventEmitter<boolean>();

  public showPaginator       :  boolean  =  false;
  public displayedColumns    : string[]  =  ['Denominacion', 'opciones'];
  public dataSource = new MatTableDataSource<IChargesResult>([]);
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  
  constructor(
    public sfn: FunctionsService,
    private modals:MatDialog,
    private semployee: EmployeeService,
    private spinner: NgxSpinnerService
  ) {
    this.getCharges();
  }

  ngOnInit(): void {  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  closeFrm() {
    this.closeForm.emit(false);
  }

  newCharges( edit: boolean = false, item: any = null ){
    
    const body = {
      edit: edit,
      item: item
    };

    this.modals.open(NewChargesComponent, {
      data: body,
      disableClose: true
    }).afterClosed().subscribe( result => {
      if( result.ok ) {
        this.getCharges();
      }
    })
  }

  public async deleteProfile( item: IChargesResult ) {
    const head1 = 'Desea';
    const head2 = 'eliminar';
    const head3 = 'el cargo de usuario';
    const message = 'Se eliminara el cargo de usuario';
    const contine = await this.sfn.modalQuestion(head1, head2, head3, message, true);
    if(contine) {
      
      this.spinner.show();
      const id = item.idcargoempleado;
      this.semployee.deleteCharge(id).subscribe( response => {

        const ok = response.status === 200;
        if(ok) {
          this.sfn.openSnackBar('cargo de usuario eliminado con exito');
          this.getCharges();
        }
        this.spinner.hide();
      }, (e) => {
        this.spinner.hide();
        this.sfn.except(e);
      });
    }
  }

  getCharges() {

    this.spinner.show();
    this.semployee.getCharge().subscribe( response => {

      this.dataSource = new MatTableDataSource<IChargesResult>(response);
      this.dataSource.paginator = this.paginator;
      this.showPaginator = this.dataSource.data.length > 10;
      if(this.showPaginator) this.paginator._intl.itemsPerPageLabel = 'items por pagina';
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  searchCharges( value: string ) {
    this.dataSource.filter = value;
    this.showPaginator = this.dataSource.filteredData.length > 10;
  }
 
}
