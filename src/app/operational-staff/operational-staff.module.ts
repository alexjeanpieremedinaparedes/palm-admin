import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../material-module';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { OperationalStaffRoutingModule } from './operational-staff-routing.module';
import { ListOperationalStaffComponent } from './pages/list-operational-staff/list-operational-staff.component';
import { NewOperationalStaffComponent } from './pages/new-operational-staff/new-operational-staff.component';
import { ListChargesComponent } from './pages/list-charges/list-charges.component';
import { NewChargesComponent } from './pages/new-charges/new-charges.component';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from '../core/core.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [    
    ListOperationalStaffComponent,
    NewOperationalStaffComponent,
    ListChargesComponent,
    NewChargesComponent
  ],
  imports: [
    CommonModule,
    OperationalStaffRoutingModule,
    SharedModule,
    MaterialModule,
    HttpClientModule,
    AngularSvgIconModule.forRoot(),  
    CoreModule,
    ReactiveFormsModule
  ]
})
export class OperationalStaffModule { }
