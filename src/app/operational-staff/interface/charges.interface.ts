
export interface IEmployee {
    status:  number;
    message: string;
    result:  IEmployeeResult[];
}

export interface IEmployeeResult {
    idempleado:      number;
    idcargo:         number;
    idtipodocumento: number;
    cargo:           string;
    tipodocumento:   string;
    numerodocumento: string;
    nombre:          string;
    apellidos:       string;
    nombrecompleto:  string;
    fechanacimiento: string;
    sexo:            boolean;
    direcion:        string;
    telefono:        string;
    correo:          string;
}

export interface ICharges {
    status:  number;
    message: string;
    result:  IChargesResult[];
}

export interface IChargesResult {
    idcargoempleado: number;
    cargoempleado:   string;
}
