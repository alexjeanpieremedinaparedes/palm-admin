import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Envsystem } from 'src/app/core/model/env.model';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { ICharges, IEmployee } from '../interface/charges.interface';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private env = new Envsystem();
  private file : string;
  constructor(
    private http: HttpClient,
    private sfn: FunctionsService
  ) {
    this.file = 'empleado/';
  }

  // TODO: CHARGE EMPLOYEE

  public getCharge() {
    const headers = this.sfn.heardToken();
    return this.http.get<ICharges>(`${this.env.mainUrl}${this.file}listarcargoempleado`, { headers }).pipe( map ( result => result.result ) );
  }

  public saveCharge(body: any) {
    const headers = this.sfn.heardToken();
    return this.http.post<ICharges>(`${this.env.mainUrl}${this.file}agregarcargoempleado`, body, { headers });
  }
  
  public deleteCharge(idCharge: number) {
    const headers = this.sfn.heardToken();
    const body = { idcargoempleado: idCharge }
    return this.http.post<ICharges>(`${this.env.mainUrl}${this.file}eliminarcargoempleado`, body, { headers });
  }

  // TODO: EMPLOYEE

  public getEmployee() {
    const headers = this.sfn.heardToken();
    return this.http.get<IEmployee>(`${this.env.mainUrl}${this.file}listarempleado`, { headers }).pipe( map ( result => result.result ) );
  }

  public saveEmployee(body: any) {
    const headers = this.sfn.heardToken();
    return this.http.post<IEmployee>(`${this.env.mainUrl}${this.file}agregarempleado`, body, { headers });
  }

  public deleteEmployee(idEmployee: number) {
    const headers = this.sfn.heardToken();
    const body = { idempleado: idEmployee }
    return this.http.post<IEmployee>(`${this.env.mainUrl}${this.file}eliminarempleado`, body, { headers });
  }
}
