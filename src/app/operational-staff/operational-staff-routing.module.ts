import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from '../layouts/dashboard/dashboard.component';
import { ListOperationalStaffComponent } from './pages/list-operational-staff/list-operational-staff.component';

const routes: Routes = [

  {
    path: '',
    component:  DashboardComponent,
     children : [
       {path: 'personal', component : ListOperationalStaffComponent},
       {path: '**', redirectTo: 'personal'}
     ]

  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OperationalStaffRoutingModule { }
