import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetalleHabitacionComponent } from './detalle-habitacion/detalle-habitacion.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    DetalleHabitacionComponent
  ],
  exports:[
    DetalleHabitacionComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    AngularSvgIconModule.forRoot(),
    ReactiveFormsModule
  ]
})
export class ComponentsmypageModule { }
