import { Component, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FunctionsService } from '../../../core/service/functions.service';
import { ValidationsService } from '../../../core/service/validations.service';
import { MypageService } from '../../service/mypage.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-detalle-habitacion',
  templateUrl: './detalle-habitacion.component.html',
  styleUrls: ['./detalle.css']
})
export class DetalleHabitacionComponent implements OnInit {

  @Input() public tipo: string = '';
  @Output() public responseok: any[] = [];

  public form!: FormGroup;
  public listImgs: string[] = [];

  private type!: string;

  constructor(
    public sfn: FunctionsService,
    private sval: ValidationsService,
    private smypage: MypageService,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService
  ) {
    this.loadImgsDefault();
  }

  ngOnInit(): void {
    this.form.valueChanges.subscribe(result => {
      this.responseok = result;
      console.log(this.responseok);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {

    this.type = changes['tipo'].currentValue
    this.createForm();
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    
  }

  public get descriptionInvalid() {
    return this.sval.ctrInvalid('descripcion', this.form );
  }

  public get capacityInvalid() {
    return this.sval.ctrInvalid('capacidad', this.form );
  }

  public get priceInvalid() {
    return this.sval.ctrInvalid('precio', this.form );
  }

  private createForm() {
    this.form = this.fb.group({
      tipo        : [ this.type ],
      descripcion : [ '', [ Validators.required ]],
      capacidad   : [ '', [ Validators.required ]],
      precio      : [ '', [ Validators.required ]],
      imagen      : [ '' ]
    });
  }

  public loadImgsDefault() {
    const max = 3;
    let cant = this.listImgs.length;

    while (cant < max) {
      this.listImgs.push('');
      cant++;
    }
  }

  public async uploadImage(event: any, index: number) {
    const img = await this.sfn.uploadFileEvt(event);
    const converthtml = this.sfn.prepareBase64ImgForHtml(img);
    this.listImgs[index] = converthtml;
    this.form.get('imagen')?.setValue(this.listImgs);
  }

  public removeImg(index: number) {
    this.listImgs[index] = '';
  }

}
