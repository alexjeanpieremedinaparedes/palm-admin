import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { BedroomService } from 'src/app/bedrooms/service/bedroom.service';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { ValidationsService } from 'src/app/core/service/validations.service';
import { IMypageResult } from '../../interfaces/mypage.interface';
import { MypageService } from '../../service/mypage.service';
import { ICategoryImagene, ICategoryResult } from '../../../bedrooms/interface/bedroom.interface';
declare  function  custoFunctions():any; 
import { Editor,Toolbar } from 'ngx-editor';
@Component({
  selector: 'app-page-bedrooms',
  templateUrl: './page-bedrooms.component.html',
  styleUrls: ['./page-bedrooms.component.css']
})
export class PageBedroomsComponent implements OnInit, OnDestroy {
 
  public listEditors: Editor[] = []
  public editInfo!: boolean;
  public editdetails!: boolean;

  public listmyPages: IMypageResult[] = [];
  public imgbs64Import!: string;

  public form!: FormGroup;
  
  // TODO: detalle de habitaciones
  public listCategory: ICategoryResult[] = []

  constructor(
    public sfn: FunctionsService,
    private spinner: NgxSpinnerService,
    private sval: ValidationsService,
    private smypage: MypageService,
    private shabitacion: BedroomService,
    private fb: FormBuilder,
  ) {
    this.getCategory()
    this.getmypages();
    this.createForm();
  }

  ngOnInit(): void {
    custoFunctions();
  }

  toolbar: Toolbar = [
    ["bold", "italic"],
    // ["underline", "strike"],
    // ["code", "blockquote"],
    // ["ordered_list", "bullet_list"],
    // [{ heading: ["h1", "h2", "h3", "h4", "h5", "h6"] }],
    // ["link", "image"],
    //  ["text_color", "background_color"],
  //  [ "align_justify"]
  ];

  public get titleInvalid() {
    return this.sval.ctrInvalid('titulo', this.form);
  }

  public get descriptionInvalid() {
    return this.sval.ctrInvalid('descripcion', this.form);
  }

  public get imgbs64Invalid() {
    return this.sval.ctrInvalid('imgbase64', this.form);
  }

  public get urlimgInvalid() {
    return this.sval.ctrInvalid('urlimg', this.form);
  }

  public createForm() {
    this.form = this.fb.group({
      idpagina    : [ 0],
      titulo      : [ '', [ Validators.required ] ],
      descripcion : [ '', [ Validators.required ] ],
      imgbase64   : [ '' ],
      urlimg      : [ '' ],
    });
  }

  ngOnDestroy(): void {

    const cant = this.listEditors.length;
    for(let i = 0; i < cant; i++) { this.listEditors[i].destroy(); };
    this.listEditors = [];
  }

  
  public pushEditors(cant: number) {
    for(let i = 0; i < cant; i++) {
      const editor = new Editor();
      this.listEditors.push(editor);
    };
  }

  public getmypages() {

    this.spinner.show();
    this.smypage.getPage().subscribe( response => {

      response.forEach(el => {
        // TODO: Cambiar el valor a las images
        if(!el.imagen.includes('https://')) {
          el.imagen = this.sfn.prepareBase64ImgForHtml(el.imagen);
        }
      });
      this.listmyPages = response;
      this.filterpage();
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  public savePage() {

    if(this.form.invalid) {
      return this.sval.emptyData(this.form);
    }

    this.spinner.show();
    const body = { ... this.form.value };
    this.smypage.savePage(body).subscribe(response =>{

      const ok = response.status === 200;
      if(ok) {
        const message = 'La información se actulizo de manera correcta';
        this.sfn.openSnackBar(message);
        this.editInfo = false;
      }

      this.spinner.hide();
    }, (e) =>{
      this.spinner.hide();
      this.sfn.except(e);
    })
  }

  public filterpage() {
    const page = 'Habitaciones';
    const item = this.listmyPages.filter(x => x.pagina === page)[0];
    this.form.patchValue({
      idpagina: item.idpagina,
      titulo: item.titulo,
      descripcion: item.descripcion,
      urlimg: item.imagen
    });

    this.imgbs64Import = item.imagen;
    console.log(item.imagen);
  }

  public removeImg() {
    this.imgbs64Import = '';
    this.form.patchValue({
      imgbase64: '',
      urlimg: ''
    });
  }

  public async importImg(event: any) {
    const bs64Img = await this.sfn.uploadFileEvt(event);
    this.imgbs64Import = this.sfn.prepareBase64ImgForHtml(bs64Img);
    this.form.patchValue({ imgbase64: bs64Img });
  }

  public cancelHeaders() {
    this.editInfo = false;
    this.getmypages();
  }

  // TODO: Detalle Habitaciones

  public changeExpanded(item: ICategoryResult) {

    this.listCategory.forEach(el => {
      if(el.idcategoriahabitacion !== item.idcategoriahabitacion) el.expanded = false;
    });
    
    item.expanded = !item.expanded;
  }

  private getCategory() {

    const max = 3;
    this.spinner.show();
    this.shabitacion.getCategoryBedroom().subscribe( response => {
      

      const cant = response.length;
      this.pushEditors(cant);
      this.listCategory = response;
      this.listCategory.forEach((el, i) => {

        // TODO: Cambiar el valor a las images
        el.imagenes.forEach(el => {
          if(!el.imagenurl.includes('https://')) {
            el.imagenurl = this.sfn.prepareBase64ImgForHtml(el.imagenurl);
          }
        });

        // el.expanded = i == 0;
        el.expanded = false;
        let cant = el.imagenes.length;
        
        while (cant < max) {

          const body = {
            idcategoriaimagen: cant + 1,
            imagenurl: '',
            predetarminado: false
          };

          el.imagenes.push(body);
          cant++;
        }
      });

      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  public async uploadImagenDatails(event: any, item: ICategoryImagene) {
    debugger;
    const img = await this.sfn.uploadFileEvt(event);

    if(img == "error")
    {
      const e:any = null;
      await this.sfn.error("Las imagenes no pueden superar mas de 850 KB");
    }
    else{
      // console.log(img);
  
      const converthtml = this.sfn.prepareBase64ImgForHtml(img);
      item.imagenurl = converthtml;
    }
  }

  public removeImgDetails(item: ICategoryResult, indexImg: number) {
    item.imagenes[indexImg].imagenurl = '';
  }

  public saveDetails(form: any) {

    if(form.invalid) {
      return this.sval.emptyData(form);
    }

    let arrayCategory: any[] = [];
    this.listCategory.forEach(el => {

      let arrayImg: any[] = [];
      el.imagenes.forEach(elImgs => {

        const body = {
          idcategoriaimagen: elImgs.idcategoriaimagen,
          urlimg: elImgs.imagenurl.includes('base64') ? '' : elImgs.imagenurl,
          predetarminado: elImgs.predetarminado,
          imgbase64: elImgs.imagenurl.includes('base64') ? elImgs.imagenurl.split(',')[1] : '',
        };
        
        arrayImg.push(body);
      });

      const body = {
        idcategoria: el.idcategoriahabitacion,
        precio: el.precio,
        descripcion: el.descripcion,
        capacidad: el.capacidad,
        detalle: arrayImg
      }
      arrayCategory.push(body);
    });

    this.spinner.show();
    
    const body = {
      categoria: arrayCategory
    }

    this.shabitacion.saveCategoryImagen(body).subscribe(response =>{
      const ok = response.status === 200;
      if(ok) {
        const message = 'La información se actulizo de manera correcta';
        this.sfn.openSnackBar(message);
        this.editdetails = false;
        this.listCategory.forEach(el => {
         el.expanded = false;
        });
       

      }

      this.spinner.hide();
    }, (e) =>{
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  public cancelDetails() {
    this.editdetails = false;
    this.getCategory();
  }

  editar()  {
    this.listCategory[0].expanded = true;
  }



  icon: boolean = false;

}
