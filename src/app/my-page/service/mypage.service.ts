import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Envsystem } from 'src/app/core/model/env.model';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { IMypage } from '../interfaces/mypage.interface';

@Injectable({
  providedIn: 'root'
})
export class MypageService {

  private env = new Envsystem();
  private file : string;
  constructor(
    private http: HttpClient,
    private sfn: FunctionsService
  ) {
    this.file = 'pagina/';
  }

  // TODO: MY PAGE

  public getPage() {
    const headers = this.sfn.heardToken();
    return this.http.get<IMypage>(`${this.env.mainUrl}${this.file}listarpagina`, { headers }).pipe( map ( result => result.result ) );
  }

  public savePage(body: any) {
    const headers = this.sfn.heardToken();
    return this.http.post<IMypage>(`${this.env.mainUrl}${this.file}editarpagina`, body, { headers });
  }
  
}
