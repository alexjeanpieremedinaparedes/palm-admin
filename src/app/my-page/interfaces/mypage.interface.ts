
export interface IMypage {
    status:  number;
    message: string;
    result:  IMypageResult[];
}

export interface IMypageResult {
    idpagina:    number;
    pagina:      string;
    titulo:      string;
    descripcion: string;
    imagen:      string;
}
