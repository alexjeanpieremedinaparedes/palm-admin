import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from '../layouts/dashboard/dashboard.component';
import { PageBedroomsComponent } from './pages/page-bedrooms/page-bedrooms.component';


const routes: Routes = [
 
 

  {
    path: '',
    component:  DashboardComponent,
     children : [
      {path: 'pagebedrooms', component : PageBedroomsComponent},
       {path: '**', redirectTo: 'pagebedrooms'}
     ]

  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyPageRoutingModule { }
