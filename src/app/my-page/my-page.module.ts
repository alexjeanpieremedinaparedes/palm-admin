import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyPageRoutingModule } from './my-page-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { MaterialModule } from '../material-module';
import { CoreModule } from '../core/core.module';
import { ComponentsmypageModule } from './components/components.module';
import { PageBedroomsComponent } from './pages/page-bedrooms/page-bedrooms.component';
import { NosotrosComponent } from './pages/nosotros/nosotros.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxEditorModule } from 'ngx-editor';

@NgModule({
  declarations: [
    PageBedroomsComponent,
    NosotrosComponent
  ],
  imports: [
    CommonModule,
    MyPageRoutingModule,
    HttpClientModule,
    AngularSvgIconModule.forRoot(),
    MaterialModule,
    CoreModule,
    ComponentsmypageModule,
    ReactiveFormsModule,
    FormsModule,
    NgxEditorModule
  ]
})
export class MyPageModule { }
