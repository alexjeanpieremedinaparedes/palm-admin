import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BedroomsRoutingModule } from './bedrooms-routing.module';
import { ListBedroomsComponent } from './pages/list-bedrooms/list-bedrooms.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { MaterialModule } from '../material-module';
import { NewBedroomsComponent } from './pages/new-bedrooms/new-bedrooms.component';
import { NewcategoryComponent } from './pages/newcategory/newcategory.component';
import { NewtypeComponent } from './pages/newtype/newtype.component';
import { CoreModule } from '../core/core.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

@NgModule({
  declarations: [
    ListBedroomsComponent,
    NewBedroomsComponent,
    NewcategoryComponent,
    NewtypeComponent
  ],

  imports: [
    CommonModule,
    BedroomsRoutingModule,
    HttpClientModule,
    AngularSvgIconModule.forRoot(),
    ReactiveFormsModule,
    MaterialModule,
    CoreModule,
    NgxMatSelectSearchModule
  ]
  
})
export class BedroomsModule { }
