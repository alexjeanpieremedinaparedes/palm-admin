import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { ValidationsService } from 'src/app/core/service/validations.service';
import { ICategoryResult, ICategoryResultServicio } from '../../interface/bedroom.interface';
import { BedroomService } from '../../service/bedroom.service';

@Component({
  selector: 'app-newcategory',
  templateUrl: './newcategory.component.html',
  styleUrls: ['./newcategory.component.css']
})
export class NewcategoryComponent implements OnInit {

  public listService: ICategoryResultServicio[] = [];
  public form!: FormGroup;
  constructor(
    public sfn: FunctionsService,
    private sval: ValidationsService,
    private sbedroom: BedroomService,
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private close: MatDialogRef<null>,
    @Inject(MAT_DIALOG_DATA) public body: any
  ) {
    this.createForm();
    if(this.body.edit) this.editForm();
    else this.getTypeService();
  }

  ngOnInit(): void {
  }

  public get categoriaInvalid() {
    return this.sval.ctrInvalid('categoria', this.form);
  }

  public get precioInvalid() {
    return this.sval.ctrInvalid('precio', this.form);
  }

  private createForm() {
    this.form = this.fb.group({
      idcategoria : [ 0 ],
      categoria   : [ '', [ Validators.required ] ],
      precio      : [ '', [ Validators.required ] ],
      editar      : [ false ]
    });
  }

  private editForm() {
    const item: ICategoryResult = this.body.item;
    this.form.patchValue({
      idcategoria : item.idcategoriahabitacion,
      categoria   : item.categoria,
      precio      : item.precio,
      editar      : true
    });

    this.listService = item.servicios;
  }

  public changeStatus( check: boolean, item: ICategoryResultServicio) {
    item.estado = check;
  }

  public getTypeService() {

    this.spinner.show();
    this.sbedroom.getServiceBedroom().subscribe( response => {
      
      this.listService = response;
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  public saveCategory() {

    if(this.form.invalid) {
      return this.sval.emptyData(this.form);
    }

    this.spinner.show();
    const permisos = this.listService.filter(x=>x.estado);
    const body = {
      ... this.form.value,
      categoriapermisos: permisos
    };

    this.sbedroom.setCategoryBedroom(body).subscribe(response =>{

      const ok = response.status === 200;
      if(ok) {
        const status = this.body.edit ? 'editado' : 'agregado';
        this.sfn.openSnackBar(`Categoría ${status} con exito`);
        this.close.close({ ok: true });
      }
      this.spinner.hide();
    }, (e)=>{
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

}
