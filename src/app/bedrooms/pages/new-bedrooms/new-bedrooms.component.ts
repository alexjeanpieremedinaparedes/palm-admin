import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { ValidationsService } from 'src/app/core/service/validations.service';
import { IBedroomResult, ICategoryResult, ITypeBedroomResult } from '../../interface/bedroom.interface';
import { BedroomService } from '../../service/bedroom.service';
import { NewcategoryComponent } from '../newcategory/newcategory.component';
import { NewtypeComponent } from '../newtype/newtype.component';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';

@Component({
  selector: 'app-new-bedrooms',
  templateUrl: './new-bedrooms.component.html',
  styleUrls: ['./new-bedrooms.component.css']
})
export class NewBedroomsComponent implements OnInit {

  @Input() body: any;
  @Output() closeForm  = new EventEmitter<boolean>();
  @Output() responseok = new EventEmitter<boolean>();

  public form!: FormGroup;
  public registrarcontrato: boolean = false;
  public selectable: boolean = true;
  public removable: boolean = true;
  public addOnBlur: boolean = true;

  public listCategory: ICategoryResult [] = [];
  public listType: ITypeBedroomResult[] = [];
  public listEtiqueta: string[] = [];

  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  constructor(
    public sfn: FunctionsService,
    private modals: MatDialog,
    private sbedroom: BedroomService,
    private spinner: NgxSpinnerService,
    private sval: ValidationsService,
    private fb: FormBuilder
  )
  {
    this.getCategory();
    this.getTypeHabitacion();
    this.createForm();
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    const edit = changes['body'].currentValue.edit;
    const item: IBedroomResult = changes['body'].currentValue.item;
    if(edit) {
      this.editForm(item);
    }
  }

  public get idcategoriaInvalid() {
    return this.sval.ctrInvalid('idcategoria', this.form);
  }

  public get idtipohabitacionInvalid() {
    return this.sval.ctrInvalid('idtipohabitacion', this.form);
  }

  public get numeracionInvalid() {
    return this.sval.ctrInvalid('numeracion', this.form);
  }

  public get camasextrasInvalid() {
    return this.sval.ctrInvalid('camasextras', this.form);
  }

  public get nivelInvalid() {
    return this.sval.ctrInvalid('nivel', this.form);
  }


  public createForm() {

    this.form = this.fb.group({
      idhabitacion     : [ 0 ],
      idcategoria      : [ '', [ Validators.required ] ],
      idtipohabitacion : [ '', [ Validators.required ] ],
      numeracion       : [ '', [ Validators.required ] ],
      camasextras      : [ '' ],
      nivel            : [ '' ],
      etiqueta         : [ '' ],
      editar           : [ false ],
      masCamas         : [ false ]
    });
  }

  public editForm(item: IBedroomResult) {

    this.listEtiqueta = !item.etiqueta ? [] : item.etiqueta.split(',');
    this.form.patchValue({
      idhabitacion     : item.idhabitacion,
      idcategoria      : item.idcategoria,
      idtipohabitacion : item.idtipohabitacion,
      numeracion       : item.numeracion,
      camasextras      : item.camasextras,
      nivel            : item.nivel,
      etiqueta         : item.etiqueta,
      masCamas         : item.camasextras > 0,
      editar           : true,
    });
  }

  public onOpenCategory(){

    const body = {
      edit: false,
      item: null
    };
    this.modals.open(NewcategoryComponent, {
      data: body,
      disableClose: true
    }).afterClosed().subscribe( response =>{
      if(response.ok) {
        this.getCategory();
      }
    });
  }

  public onOpenType(){

    const body = {
      edit: false,
      item: null
    };

    this.modals.open(NewtypeComponent, {
      data: body,
      disableClose: true
    }).afterClosed().subscribe( response =>{
      if(response.ok) {
        this.getTypeHabitacion();
      }
    });
  }

  public closeFrm() {
    this.closeForm.emit(false);
  }

  public changeMasCamas() {

    const masCamas = this.form.value.masCamas;
    this.form.get('camasextras')?.setValue('');
    if(masCamas) this.form.get('camasextras')?.setValidators(Validators.required);
    else this.form.get('camasextras')?.clearValidators();
    this.form.get('camasextras')?.updateValueAndValidity();
  }

  private getCategory() {

    this.spinner.show();
    this.sbedroom.getCategoryBedroom().subscribe( response => {
      
      this.listCategory = response;
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  private getTypeHabitacion() {

    this.spinner.show();
    this.sbedroom.getTypeBedroom().subscribe( response => {
      
      this.listType = response;
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  public saveBedroom() {

    if(this.form.invalid) {
      return this.sval.emptyData(this.form);
    }

    this.spinner.show();
    const body = { ... this.form.value };
    body.etiqueta = this.listEtiqueta.join(',');
    body.camasextras = !body.camasextras ? 0 : Number(body.camasextras);
    delete body.masCamas;
    this.sbedroom.setBedroom(body).subscribe( response =>{

      const ok = response.status === 200;
      if(ok) {
        const status = this.body.edit ? 'editado' : 'agregado';
        this.sfn.openSnackBar(`Habitación ${status} con exito`);
        this.responseok.emit(true);
        this.closeFrm();
      }
      this.spinner.hide();
    }, (e)=>{
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  public add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value) {
      this.listEtiqueta.push(value);
    }
    event.chipInput!.clear();
  }

  public remove(index: number): void {
    this.listEtiqueta.splice(index, 1);
  }
}
