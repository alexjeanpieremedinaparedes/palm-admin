import {  Component, OnInit, ViewChild  } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { SettingService } from 'src/app/core/service/setting.service';
import { IBedroomResult, ICategoryResult, ICategoryResultServicio, ITypeBedroomResult } from '../../interface/bedroom.interface';
import { BedroomService } from '../../service/bedroom.service';
import { NewcategoryComponent } from '../newcategory/newcategory.component';
import { NewtypeComponent } from '../newtype/newtype.component';

@Component({
  selector: 'app-list-bedrooms',
  templateUrl: './list-bedrooms.component.html',
  styleUrls: ['./list-bedrooms.component.css']
})
export class ListBedroomsComponent implements OnInit {
  openTab = 1;
  toggleTabs($tabNumber: number){
    this.openTab = $tabNumber;
  }

  // public closeFrm() {
  //   this.closeForm.emit(false);
  // }
  @ViewChild(MatPaginator)   public paginatorGeneral!: MatPaginator;
  @ViewChild('pageCategory') public paginatorCategory!: MatPaginator;
  @ViewChild('pageType')     public paginatorType!: MatPaginator;
  @ViewChild('pgService')    public paginatorService!: MatPaginator;
  
  public body: any;
  public columnsBedrom: string[] = ['tipnum', 'cate', 'niv', 'eti', 'pre', 'cap', 'est',  'opciones'  ];
  public dataSourceBedrom = new MatTableDataSource<IBedroomResult>([]);

  //................................................................ DATA DE PROPIEDADES.......................................................................
  // CategoriA
  public columnsCategory: string[] = ['deno', 'prec', 'servic', 'acc'];
  public dataSourceCategory = new MatTableDataSource<ICategoryResult>([]);

  // Tipo habitacion
  public columnsTypeHabitacion: string[] = ['nom', 'capac', 'acc'];
  public dataSourceTypeHabitacion = new MatTableDataSource<ITypeBedroomResult>([]);

  // Listado de servicio
  public columnsService: string[] = ['ico', 'nom'];
  public dataSourceService = new MatTableDataSource<ICategoryResultServicio>([]);

  public openHabitacion: boolean = false;

  public abrirpropiedades = false;
  public cerrar=false;

  public showPageGeneral: boolean = false;
  public showPageCategory: boolean = false;
  public showPageType: boolean = false;
  public showPageService: boolean = false;

  constructor(
    public sfn: FunctionsService,
    private modals: MatDialog,
    private sbedroom: BedroomService,
    private spinner: NgxSpinnerService,
    private ssetting: SettingService,
    private router: Router
  ) {
    this.getBedroom();
    this.getCategory();
    this.getTypeHabitacion();
    this.getTypeService();
  }

  ngOnInit(): void {

    this.dataSourceBedrom.paginator = this.paginatorGeneral;
    this.ssetting.textSearch$.subscribe( response => {
      const urlActual = this.router.url;
      const urlReceived = response.url;
      if( urlActual === urlReceived ) {
        this.dataSourceBedrom.filter = response.value;
      }
    });
  }

  public editBedroom( edit: boolean, item: any = null  ) {
    this.openHabitacion = true;
    this.body = {
      edit: edit,
      item: item
    }
  }

  public CerrarNuevoEntradaMercaderia()
  {
    this.cerrar=true;
    setTimeout(() => {
      this.abrirpropiedades = false;
      this.cerrar = false;
    }, 1000);
  }

  public onEditCategory(edit: boolean, item: any = null){

    const body = {
      edit: edit,
      item: item
    };
    this.modals.open(NewcategoryComponent, {
      data: body,
      disableClose: true
    }).afterClosed().subscribe( response =>{
      if(response.ok) {
        this.getCategory();
      }
    });
  }

  public onEditType(edit: boolean, item: any = null){

    const body = {
      edit: edit,
      item: item
    };

    this.modals.open(NewtypeComponent, {
      data: body,
      disableClose: true
    }).afterClosed().subscribe( response =>{
      if(response.ok) {
        this.getTypeHabitacion();
      }
    });
  }

  public getBedroom() {

    this.spinner.show();
    this.sbedroom.getBedroom().subscribe( response => {
      
      this.dataSourceBedrom = new MatTableDataSource<IBedroomResult>(response);
      this.dataSourceBedrom.paginator = this.paginatorGeneral;
      this.showPageGeneral = this.dataSourceBedrom.data.length > 10;
      if(this.showPageGeneral) this.paginatorGeneral._intl.itemsPerPageLabel = 'items por pagina';
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  public getCategory() {

    this.spinner.show();
    this.sbedroom.getCategoryBedroom().subscribe( response => {
      
      this.dataSourceCategory = new MatTableDataSource<ICategoryResult>(response);
      this.dataSourceCategory.paginator = this.paginatorCategory;
      this.showPageCategory = this.dataSourceCategory.data.length > 10;
      if(this.showPageCategory) this.paginatorCategory._intl.itemsPerPageLabel = 'items por pagina';
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  public getTypeHabitacion() {

    this.spinner.show();
    this.sbedroom.getTypeBedroom().subscribe( response => {
      
      this.dataSourceTypeHabitacion = new MatTableDataSource<ITypeBedroomResult>(response);
      this.dataSourceTypeHabitacion.paginator = this.paginatorType;
      this.showPageType = this.dataSourceTypeHabitacion.data.length > 10;
      if(this.showPageType) this.paginatorType._intl.itemsPerPageLabel = 'items por pagina';
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });
  }
  public getTypeService() {

    this.spinner.show();
    this.sbedroom.getServiceBedroom().subscribe( response => {
      
      console.log(response.length);

      this.dataSourceService = new MatTableDataSource<ICategoryResultServicio>(response);
      this.dataSourceService.paginator = this.paginatorService;
      this.showPageService = this.dataSourceService.data.length > 10;
      // if(this.showPageService) this.paginatorService._intl.itemsPerPageLabel = 'items por pagina';
      this.spinner.hide();

    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  public async deleteBedroom( item: IBedroomResult ) {
    const head1 = 'Desea';
    const head2 = 'eliminar';
    const head3 = 'la habitación';
    const message = 'Se eliminara la habitación';
    const contine = await this.sfn.modalQuestion(head1, head2, head3, message, true);
    if(contine) {
      
      this.spinner.show();
      const id = item.idhabitacion;
      this.sbedroom.deleteBedroom(id).subscribe( response => {

        const ok = response.status === 200;
        if(ok) {
          this.sfn.openSnackBar('Habitación eliminada con exito');
          this.getBedroom();
        }
        this.spinner.hide();
      }, (e) => {
        this.spinner.hide();
        this.sfn.except(e);
      });
    }
  }


  public async deleteCategory( item: ICategoryResult ) {
    const head1 = 'Desea';
    const head2 = 'eliminar';
    const head3 = 'la categoría de la habitación';
    const message = 'Se eliminara la categoría de la habitación';
    const contine = await this.sfn.modalQuestion(head1, head2, head3, message, true);
    if(contine) {
      
      this.spinner.show();
      const id = item.idcategoriahabitacion;
      this.sbedroom.deleteCategoryBedroom(id).subscribe( response => {

        const ok = response.status === 200;
        if(ok) {
          this.sfn.openSnackBar('Categoría eliminada con exito');
          this.getCategory();
        }
        this.spinner.hide();
      }, (e) => {
        this.spinner.hide();
        this.sfn.except(e);
      });
    }
  }

  public async deleteTypeHabitacion( item: ITypeBedroomResult ) {
    const head1 = 'Desea';
    const head2 = 'eliminar';
    const head3 = 'el tipo de habitación';
    const message = 'Se eliminara el tipo de habitación';
    const contine = await this.sfn.modalQuestion(head1, head2, head3, message, true);
    if(contine) {
      
      this.spinner.show();
      const id = item.idtipohabitacion;
      this.sbedroom.deleteTypeBedroom(id).subscribe( response => {

        const ok = response.status === 200;
        if(ok) {
          this.sfn.openSnackBar('Tipo de habitación eliminada con exito');
          this.getTypeHabitacion();
        }
        this.spinner.hide();
      }, (e) => {
        this.spinner.hide();
        this.sfn.except(e);
      });
    }
  }

  public async activeBedroom( item: IBedroomResult ) {

    const status = item.estado === 'ACTIVO' ? 'Desactivar' : 'Activar';
    const statusms = item.estado === 'ACTIVO' ? 'desactivará' : 'activará';
    const danger = item.estado === 'ACTIVO';
    const head1 = 'Desea';
    const head2 = status;
    const head3 = 'la habitación';
    const message = `Se ${statusms} la habitación`;
    const contine = await this.sfn.modalQuestion(head1, head2, head3, message, danger);
    if(contine) {
      
      this.spinner.show();
      const id = item.idhabitacion;
      this.sbedroom.setStatusBedroom(id).subscribe( (response: any) => {

        const ok = response.status === 200;
        if(ok) {
          const result = response.result;
          const status = result.estado === 'ACTIVO' ? 'Activado' : 'Desactivado';
          this.sfn.openSnackBar(`Habitación ${status}`);
          this.getBedroom();
        }
        this.spinner.hide();
      }, (e) => {
        this.spinner.hide();
        this.sfn.except(e);
      });
    }
  }

}
