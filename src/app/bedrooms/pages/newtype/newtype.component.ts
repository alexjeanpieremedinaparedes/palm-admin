import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { ValidationsService } from 'src/app/core/service/validations.service';
import { ITypeBedroomResult } from '../../interface/bedroom.interface';
import { BedroomService } from '../../service/bedroom.service';

@Component({
  selector: 'app-newtype',
  templateUrl: './newtype.component.html',
  styleUrls: ['./newtype.component.css']
})
export class NewtypeComponent implements OnInit {

  public form!: FormGroup;
  constructor(
    public sfn: FunctionsService,
    private sval: ValidationsService,
    private sbedroom: BedroomService,
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private close: MatDialogRef<null>,
    @Inject(MAT_DIALOG_DATA) public body: any
  ) {
    this.createForm();
    if(body.edit) this.editForm();
  }

  ngOnInit(): void {
  }

  public get tipoInvalid() {
    return this.sval.ctrInvalid('tipohabitacion', this.form);
  }
  
  public get personasInvalid() {
    return this.sval.ctrInvalid('personas', this.form);
  }

  private createForm() {
    this.form = this.fb.group({
      idtipohabitacion : [ 0 ],
      tipohabitacion   : [ '', [ Validators.required ] ],
      personas         : [ '', [ Validators.required ] ],
      editar           : [ false ]
    });
  }

  private editForm() {
    const item: ITypeBedroomResult = this.body.item;
    this.form.patchValue({
      idtipohabitacion : item.idtipohabitacion,
      tipohabitacion   : item.tipohabitacion,
      personas         : item.personas,
      editar           : true
    });
  }

  public saveTypeBedroom() {

    if(this.form.invalid) {
      return this.sval.emptyData(this.form);
    }

    this.spinner.show();
    const body = { ... this.form.value };
    this.sbedroom.setTypeBedroom(body).subscribe( response =>{

      const ok = response.status === 200;
      if(ok) {
        const status = this.body.edit ? 'editado' : 'agregado';
        this.sfn.openSnackBar(`Tipo habitación ${status} con exito`);
        this.close.close({ ok: true });
      }
      this.spinner.hide();
    }, (e)=>{
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

}
