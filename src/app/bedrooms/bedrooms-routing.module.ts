import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardComponent } from '../layouts/dashboard/dashboard.component';
import { ListBedroomsComponent } from './pages/list-bedrooms/list-bedrooms.component';

const routes: Routes = [
  {
    path: '',
    component:  DashboardComponent,
     children : [
       {path: 'habitaciones', component : ListBedroomsComponent},
       {path: '**', redirectTo: 'habitaciones'}
     ]

  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BedroomsRoutingModule {}
