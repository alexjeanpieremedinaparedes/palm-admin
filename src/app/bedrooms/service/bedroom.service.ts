import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Envsystem } from 'src/app/core/model/env.model';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { IBedroom, ICategory, ISeviceBedroom, ITypeBedroom } from '../interface/bedroom.interface';
@Injectable({
  providedIn: 'root'
})
export class BedroomService {

  private env = new Envsystem();
  private file : string;
  constructor
  (
    private http: HttpClient,
    private fn: FunctionsService
  )

  {
    this.file = 'habitacion/';
  }

  public getBedroom() {
    return this.http.get<IBedroom>(`${this.env.mainUrl}${this.file}listarhabitacion`,{ headers: this.fn.heardToken() }).pipe( map( result => result.result ));
  }
  
  public getCategoryBedroom() {
    const headers = this.fn.heardToken();
    return this.http.get<ICategory>(`${this.env.mainUrl}${this.file}listarcategoriahabitacion`,{ headers }).pipe( map( result => result.result ));
  }

  public getTypeBedroom() {
    const headers = this.fn.heardToken();
    return this.http.get<ITypeBedroom>(`${this.env.mainUrl}${this.file}listartipohabitacion`,{ headers }).pipe( map( result => result.result ));
  }

  public getServiceBedroom() {
    const headers = this.fn.heardToken();
    return this.http.get<ISeviceBedroom>(`${this.env.mainUrl}${this.file}listarserviciohabitacion`,{ headers }).pipe( map( result => result.result ));
  }

  public setBedroom(body:any) {
    const headers = this.fn.heardToken();
    return this.http.post<IBedroom>(`${this.env.mainUrl}${this.file}agregarhabitacion`,body ,{ headers });
  }

  public setStatusBedroom(idHabitacion: number) {

    const headers = this.fn.heardToken();
    const body = { idhabitacion: idHabitacion };
    return this.http.post<IBedroom>(`${this.env.mainUrl}${this.file}activardesactivarhabitacion`,body ,{ headers });
  }

  public setCategoryBedroom(body:any) {

    const headers = this.fn.heardToken();
    return this.http.post<ICategory>(`${this.env.mainUrl}${this.file}agregarcategoriahabitacion`,body ,{ headers });
  }

  public setTypeBedroom(body:any) {
    const headers = this.fn.heardToken();
    return this.http.post<ITypeBedroom>(`${this.env.mainUrl}${this.file}agregartipohabitacion`,body ,{ headers });
  }

  public deleteBedroom(idHabitacion: number) {

    const headers = this.fn.heardToken();
    const body = { idhabitacion: idHabitacion };
    return this.http.post<IBedroom>(`${this.env.mainUrl}${this.file}eliminarhabitacion`,body ,{ headers });
  }

  public deleteCategoryBedroom(idtipo: number) {

    const headers = this.fn.heardToken();
    const body = { idcategoria: idtipo };
    return this.http.post<ICategory>(`${this.env.mainUrl}${this.file}eliminarcategoriahabitacion`,body ,{ headers });
  }

  public deleteTypeBedroom(idType: number) {
    
    const headers = this.fn.heardToken();
    const body = { idtipohabitacion: idType };
    return this.http.post<ITypeBedroom>(`${this.env.mainUrl}${this.file}eliminartipohabitacion`,body ,{ headers });
  }

  public saveCategoryImagen(body: any) {
    const headers = this.fn.heardToken();
    return this.http.post<ICategory>(`${this.env.mainUrl}${this.file}editarcategoriaimagen`,body ,{ headers });
  }
}
