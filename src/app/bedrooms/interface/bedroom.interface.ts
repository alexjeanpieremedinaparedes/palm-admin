
// TODO: LISTAR HABITACIONES
export interface IBedroom {
  status:  number;
  message: string;
  result:  IBedroomResult[];
}

export interface IBedroomResult {
  idhabitacion:     number;
  idtipohabitacion: number;
  idcategoria:      number;
  tipo:             string;
  numeracion:       number;
  categoria:        string;
  nivel:            number;
  etiqueta:         string;
  precioregular:    number;
  capacidad:        number;
  camasextras:      number;
  estado:           string;
  mantenimiento:    boolean;
}

// TODO: CATEGORIA HABITACION

export interface ICategory {
  status:  number;
  message: string;
  result:  ICategoryResult[];
}

export interface ICategoryResult {
  idcategoriahabitacion: number;
  categoria:             string;
  capacidad:             number
  descripcion:           string;

  precio:                number;
  cantidadservicios:     number;
  imagenes:              ICategoryImagene[];
  servicios:             ICategoryResultServicio[];

  expanded: boolean;
}

export interface ISeviceBedroom {
  status:  number;
  message: string;
  result:  ICategoryResultServicio[];
}

export interface ICategoryResultServicio {
  idservicio: number;
  servicio:   string;
  estado:     boolean;
  icono:      string;
}

export interface ICategoryImagene {
  idcategoriaimagen: number;
  imagenurl:         string;
  predetarminado:    boolean;
}

// TODO: TIPO HABITACION

export interface ITypeBedroom {
  status:  number;
  message: string;
  result:  ITypeBedroomResult[];
}

export interface ITypeBedroomResult {
  idtipohabitacion: number;
  tipohabitacion:   string;
  personas:         number;
}