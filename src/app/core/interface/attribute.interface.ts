
export interface ITypeDocument {
    status:  number;
    message: string;
    result:  ITypeDocumentResult[];
}

export interface ITypeDocumentResult {
    idtipodocumento: number;
    tipodocumento:   string;
}

export interface IReniec {
    status:  number;
    message: string;
    result:  IReniecReniec;
}

export interface IReniecReniec {
    status:  string;
    message: string;
    result:  IReniecResult;
}

export interface IReniecResult {
    nuDni:               string;
    apePaterno:          string;
    apeMaterno:          string;
    preNombres:          string;
    digitoVerificacion:  string;
    nuImagen:            string;
    feNacimiento:        string;
    estatura:            string;
    sexo:                string;
    estadoCivil:         string;
    gradoInstruccion:    string;
    feEmision:           string;
    feInscripcion:       string;
    nomPadre:            string;
    nomMadre:            string;
    cancelacion:         string;
    departamento:        string;
    provincia:           string;
    distrito:            string;
    depaDireccion:       string;
    provDireccion:       string;
    distDireccion:       string;
    feFallecimiento:     string;
    depaFallecimiento:   string;
    provFallecimiento:   string;
    distFallecimiento:   string;
    feCaducidad:         string;
    donaOrganos:         string;
    observacion:         string;
    vinculoDeclarante:   string;
    nomDeclarante:       string;
    deRestriccion:       string;
    desDireccion:        string;
    imagenes:            IReniecImages;
    fecha_actualizacion: string;
}

export interface IReniecImages {
    foto:    string;
    huellai: string;
    huellad: string;
    firma:   string;
}
