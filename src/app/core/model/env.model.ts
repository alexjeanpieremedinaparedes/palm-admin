import { environment } from "src/environments/environment";

export class Envsystem {
    mainUrl: string;
    photoForDefault: string;
    dataLogin: string;
    expiredms: string;
    brokenConnectionms: string;
    keyLocalStorage: string;

    constructor() {
        this.mainUrl = environment.mainUrl;
        this.dataLogin = '_innv.datalogin_palm_ad';
        this.expiredms = 'conexión expirada, vuelva a iniciar sesión';
        this.brokenConnectionms = 'Sin conexión al servidor';
        this.photoForDefault = 'assets/img/default/photoDefault.png';
        this.keyLocalStorage = 'palmeras';
    }
}