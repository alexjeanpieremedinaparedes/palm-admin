import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { NotificationComponent } from '../components/notification/notification.component';
import { QuestionComponent } from '../components/question/question.component';
import * as CryptoJS  from 'crypto-js';
import { Envsystem } from '../model/env.model';
import { ErrorComponent } from '../components/error/error.component';

@Injectable({
  providedIn: 'root'
})
export class FunctionsService {

  private env = new Envsystem();
  constructor(
    private modal: MatDialog,
    private _snackBar: MatSnackBar,
  ) { }

  public encrypt( data: string ) {
    return CryptoJS.AES.encrypt( data, this.env.keyLocalStorage).toString();
  }

  public decrypt(data: string) {
    const values = CryptoJS.AES.decrypt(data, this.env.keyLocalStorage).toString(CryptoJS.enc.Utf8);
    return JSON.parse(values);
  }

  public groupByKey(array: any[], key: string): any[] {

    let list: any[] = [];

    const group = array
      .reduce((hash, obj) => {
        if(obj[key] === undefined) return hash;
        return Object.assign(hash, { [obj[key]]:( hash[obj[key]] || [] ).concat(obj)})
      }, {})

    const entries = Object.entries(group);
    entries.forEach( el => {
      const body = {
        group: el[0],
        all: el[1]
      };

      list.push(body);
    })

    return list;
  }

  public prepareBase64ImgForHtml(base64: string) {
    return 'data:image/jpeg;base64,' + base64;
  }

  public onFocus(focus: string) {
    document.getElementById(focus)?.focus();
  }

  public heardToken() {
    const encrypt = localStorage.getItem(this.env.dataLogin) ?? '';
    const decrypt = this.decrypt(encrypt);
    const token = decrypt.token;
    return { Authorization: `bearer ${token}` };
  }

  public except(e: any) {
    const expired = e.error === 'Unauthorized';
    const message = (expired) ? this.env.expiredms : e.error.message ?? this.env.brokenConnectionms;

    const body = {
      message: message,
      expired: expired
    };

    this.modal.open(ErrorComponent, {
      data: body,
      disableClose: true
    })
  }

  public error(mensaje: string) {
    const body = {
      message: mensaje,
      expired: false
    };

    this.modal.open(ErrorComponent, {
      data: body,
      disableClose: true
    })
  }

  public async modalQuestion( head1: string, head2: string, head3: string, message: string, danger: boolean ): Promise<boolean> {

    const body = {
      head1: head1,
      head2: head2,
      head3: head3,
      message: message,
      danger: danger
    };

    return await new Promise( (resolve, reject) => {
      this.modal.open(QuestionComponent, {
        data: body,
        disableClose: true
      }).afterClosed().subscribe( response => resolve(response) );
    });
  }

  public openSnackBar(message: string) {
    const horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    const verticalPosition: MatSnackBarVerticalPosition = 'top';
    const durationInSeconds = 5;

    this._snackBar.openFromComponent(NotificationComponent, {
      duration: durationInSeconds * 200,
      horizontalPosition: horizontalPosition,
      verticalPosition: verticalPosition,
      data: message
    });
  }

  public dateMoment( dato: any) {
    const date = new Date(dato +"T00:00:00");
    return date;
  }
  
  public onlyNumberAllowed(event: any, limit: number) : boolean {
    const charCode = (event.which)?event.which: event.keycode;
    if(charCode > 31 && (charCode < 48 || charCode > 57 )) {
      return false;
    }
  
    const len = event.target.value.length;
    if(len > (limit - 1)) return false;

    return true;
  }

  public validatePorcentual(value: string) : boolean {
    return Number(value) > 100 ? false : true;
  }

  public lenghtValue(event: any, limit: number) : boolean {

    const len = event.target.value.length;
    if(len > (limit - 1)) return false;
    return true;
  }

  public ageForDate( date: any ): number {
    const birthdate: any = date;
    const cur: any = new Date();
    const diff: any = cur-birthdate;
    return Math.floor(diff/31557600000);
  }

  public async uploadFileEvt(imgFile: any): Promise<string>  {

    return await new Promise( (resolve, reject) => {
    
      const peso = Number(Number(imgFile.target.files[0].size) / 1024).toFixed(0);

        if(Number(peso) > 850) {
          resolve("error");
        }
        else
        {
          if (imgFile.target.files && imgFile.target.files[0]) {
            
            let reader = new FileReader();
      
            reader.onload = (e: any) => {
      
              let image = new Image();
              image.src = e.target.result;
    
              image.onload = rs => {
                let imgBase64Path = e.target.result;
                const bs64split = imgBase64Path.split(',')[1];
                resolve(bs64split);
              };
            };
      
            reader.readAsDataURL(imgFile.target.files[0]);
        }
      }
    })
  }
}
