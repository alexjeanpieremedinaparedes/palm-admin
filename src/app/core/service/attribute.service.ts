import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { IReniec, ITypeDocument } from '../interface/attribute.interface';
import { Envsystem } from '../model/env.model';
import { FunctionsService } from './functions.service';

@Injectable({
  providedIn: 'root'
})
export class AttributeService {

  private env = new Envsystem();
  private file: string;
  private fileService: string;
  constructor(
    private http: HttpClient,
    private sfn: FunctionsService
  ) {
    this.file = 'atributo/';
    this.fileService = 'servicio/'
  }

  public gettypeDocument() {

    const headers = this.sfn.heardToken();
    return this.http.get<ITypeDocument>(`${this.env.mainUrl}${this.file}listartipodocumento`, { headers }).pipe( map( result => result.result ) );
  }

  public getReniec( dni: string ) {

    const headers = this.sfn.heardToken();
    const body = { dni: dni };
    return this.http.post<IReniec>(`${this.env.mainUrl}${this.fileService}buscardni`, body, { headers } );
  }
}
