import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationsService {

  constructor() { }

  emptyData( form: FormGroup ) {

    return Object.values(form.controls).forEach(control => {
      if (control instanceof FormGroup) {
        Object.values(control.controls).forEach(control => control.markAsTouched());
      }
      else {
        control.markAsTouched();
      }
    });
  }

  ctrInvalid( dato: string, form: FormGroup ) {
    return form.get(dato)?.invalid && form.get(dato)?.touched;
  }
}
