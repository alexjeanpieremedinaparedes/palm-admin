import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { LaodingComponent } from './components/laoding/laoding.component';
import { QuestionComponent } from './components/question/question.component';
import { NotificationComponent } from './components/notification/notification.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MaterialModule } from '../material-module';
import { ErrorComponent } from './components/error/error.component';
import { PhotoDeaultDirective } from './directive/photo-deault.directive';


@NgModule({
  declarations: [
    LaodingComponent,
    QuestionComponent,
    NotificationComponent,
    ErrorComponent,
    PhotoDeaultDirective
  ],

  exports:[
    LaodingComponent,
    QuestionComponent,
    NotificationComponent,
    ErrorComponent,
    PhotoDeaultDirective
  ],
  imports: [
    CommonModule,
    CoreRoutingModule,
    NgxSpinnerModule,
    MaterialModule
  ]
})
export class CoreModule { }
