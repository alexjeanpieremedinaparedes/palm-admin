
export interface IAuth {
    status:  number;
    message: string;
    result:  IAuthResult;
}

export interface IAuthResult {
    idusuario:   number;
    usuario:     string;
    ruc:         string;
    razonsocial: string;
    logo:        string;
    token:       string;
    permisos:    any[];
    empleados:   IAuthEmployee[];
}

export interface IAuthEmployee {
    numerodocumento: string;
    nombre:          string;
    apellidos:       string;
    nombrecompleto:  string;
    fechanacimiento: string;
    sexo:            boolean;
    direccion:       string;
    telefono:        string;
    correo:          string;
}