import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Envsystem } from 'src/app/core/model/env.model';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { ValidationsService } from 'src/app/core/service/validations.service';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private env = new Envsystem();
  public form!: FormGroup;
  public hide = true;
  public  year = new Date().getFullYear();

  constructor(
    public sfn: FunctionsService,
    private sval: ValidationsService,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private sauth: AuthService,
    private router: Router
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.sfn.onFocus('userLogin');
  }
 

  public get userInvalid() {
    return this.sval.ctrInvalid('usuario', this.form);
  }

  public get passInvalid() {
    return this.sval.ctrInvalid('password', this.form);
  }

  private createForm() {
    this.form = this.fb.group({
      usuario  : [ '', [ Validators.required ] ],
      password : [ '', [ Validators.required ] ]
    });
  }

  public login() {

    if(this.form.invalid) {
      this.sfn.onFocus('userLogin');
      return this.sval.emptyData(this.form);
    }

    this.spinner.show();
    const body = { ... this.form.value };
    this.sauth.login(body).subscribe( response => {      
      const ok = response.status === 200;
      if(ok) {
        const info = JSON.stringify(response.result);
        localStorage.setItem(this.env.dataLogin, this.sfn.encrypt(info));
        this.continuePage();
      }

      this.spinner.hide();
    }, (e) =>{
      this.spinner.hide();
      this.sfn.onFocus('userLogin');
      this.sfn.except(e);
    });
  }

  private continuePage() {
    this.router.navigateByUrl('/personal', { replaceUrl: true });
  }

}
