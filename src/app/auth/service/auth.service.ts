import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Envsystem } from 'src/app/core/model/env.model';
import { IAuth } from '../interface/auth.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private env = new Envsystem();
  private file : string;
  constructor( 
    private http: HttpClient,
    private router: Router
   ) {
    this.file = 'usuario/';
  }

  public login(body: any) {
    return this.http.post<IAuth>(`${this.env.mainUrl}${this.file}iniciosesion`, body);
  }

  public logout() {
    localStorage.removeItem(this.env.dataLogin);
    this.router.navigateByUrl('/login', { replaceUrl: true });
  }
}
