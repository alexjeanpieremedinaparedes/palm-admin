import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';
import { MaterialModule } from '../material-module';

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
     // svg-icon
     HttpClientModule,
     AngularSvgIconModule.forRoot(),
     ReactiveFormsModule,
     CoreModule,
     MaterialModule
  ]
})
export class AuthModule { }
