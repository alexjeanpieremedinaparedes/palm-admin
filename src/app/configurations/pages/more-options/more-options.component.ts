import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { SettingService } from 'src/app/configurations/service/setting.service';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { ValidationsService } from 'src/app/core/service/validations.service';

@Component({
  selector: 'app-more-options',
  templateUrl: './more-options.component.html',
  styleUrls: ['./more-options.component.css'],
})
export class MoreOptionsComponent implements OnInit {

  public form!: FormGroup;
  public hide = true;

  constructor(
    private fb: FormBuilder,
    private spass: SettingService,
    private sval: ValidationsService,
    private spinner: NgxSpinnerService,
    public sfn: FunctionsService,
    
  ) { 
    this.createForm();
  }


  get valorInvalid() {
    return this.sval.ctrInvalid('valor', this.form);
  }

  public createForm() {
    this.form = this.fb.group({
      configuracion: ['CONTRASEÑA SEGURIDAD'],
      valor: ['', Validators.required],
    });
  }


  savePassword() {
    
    if (this.form.invalid) {     
      return this.sval.emptyData(this.form);
    }
    
    this.spinner.show();
    const body = {... this.form.value };
    this.spass.savePassword(body).subscribe(response => {
      const ok = response.status === 200;
      if(ok) {
        this.sfn.openSnackBar('Se Guardo la Contraseña');
      }
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });

  }

  ngOnInit(): void { }
}
