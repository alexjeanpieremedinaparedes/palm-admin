import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { ValidationsService } from 'src/app/core/service/validations.service';
import { IUser } from '../../../interface/user.interface';
import { UserService } from '../../../service/user.service';

@Component({
  selector: 'app-list-usuario',
  templateUrl: './list-usuario.component.html',
  styleUrls: ['./list-usuario.component.css']
})

export class ListUsuarioComponent implements OnInit {

  form!:FormGroup;
  listUser:any;
  dataUser:any = {};

  public newUsuario: boolean = false;
  public showPaginator: boolean = false;
  public displayedColumns: string[] = ['ApellidosNombres', 'Usuario', 'Contraseña', 'permisos', 'opciones'];
  public dataSource = new MatTableDataSource<IUser>([]);
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private user: UserService,
    private spinner: NgxSpinnerService,
    private fn: FunctionsService
  )
  {
    this.getUser();
  }

  ngOnInit(): void {
  }

  newUser(id:any) {
    this.dataUser = this.listUser.find((e:any)=> e.idusuario == id);
    this.newUsuario = true;
  }

  getUser()
  {
    this.spinner.show();
    this.user.getUser().subscribe((response)=> {
      this.spinner.hide();
      this.listUser = response;
      this.dataSource = new MatTableDataSource<IUser>(response);
    },(e)=> { this.spinner.hide(); this.fn.except(e);});
  }

  async deleteUser(id:number)
  {
    const head1 = 'Desea';
    const head2 = 'eliminar';
    const head3 = 'el usuario';
    const message = 'Se eliminara el usuario';
    const contine = await this.fn.modalQuestion(head1, head2, head3, message, true);
    if(contine)
    {
      this.user.deleteUser({idusuario:id}).subscribe((response)=> {
        this.getUser();
      }, (e)=> { this.fn.except(e); })
    }
  }
}
