import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { ValidationsService } from 'src/app/core/service/validations.service';
import { EmployeeService } from 'src/app/operational-staff/service/employee.service';
import { UserService } from '../../../service/user.service';

@Component({
  selector: 'app-new-usuario',
  templateUrl: './new-usuario.component.html',
  styleUrls: ['./new-usuario.component.css']
})
export class NewUsuarioComponent implements OnInit {

  form!: FormGroup;
  employeeList!: any[];
  formList!: any[];
  idsEmployee:any = []

  @Input() body: any;
  @Input() userdata:any;
  @Output() closeForm  = new EventEmitter<boolean>();
  @Output() responseok  = new EventEmitter<boolean>();

  constructor(
    private fb: FormBuilder,
    private employee:EmployeeService,
    private user: UserService,
    private sval: ValidationsService,
    private fn: FunctionsService,
    private spinner: NgxSpinnerService
  )
  {
    this.CreateForm();
    this.getEmployee();
    this.getForm();
  }

  ngOnInit(): void {

    if(this.userdata)
    {
      let ids:any = []
      this.userdata.empleados.forEach((e:any) => {
        ids.push(e.idempleado)
      });
      this.idsEmployee = ids;
      this.form.patchValue({
        idusuario: this.userdata.idusuario,
        usuario: this.userdata.usuario,
        password: this.userdata.password,
        editar: true,
        empleadoids: ids
        //permisos: this.userdata.permisos
      });
    }
  }

  closeFrm() {
    this.closeForm.emit(false);
  }

  getEmployee()
  {
    this.employee.getEmployee().subscribe((response)=> {
      this.employeeList = response;
    });
  }

  EmployeeSelect($event:any,id:number)
  {
    if($event.source._selected) this.idsEmployee.push(id)
    else this.idsEmployee = this.idsEmployee.filter((e:any)=> { if(e.id != id) return; });
  }

  getForm()
  {
    this.user.getForm().subscribe((response)=> {
      this.formList = response;
      if(this.userdata)this.formList = this.userdata.permisos
    });
  }

  CreateForm()
  {
    this.form = this.fb.group({
      idusuario: [ 0 , Validators.required ],
      usuario: [ '' , Validators.required ],
      password: [ '' , Validators.required ],
      editar: [ false , Validators.required ],
      empleadoids: this.employeeList,
      permisos: [ [] ]
    });
  }

  get invalidUsuario(){ return this.sval.ctrInvalid('usuario',this.form)}
  get invalidPassword(){ return this.sval.ctrInvalid('password',this.form)}

  saveUser()
  {
    if(this.form.invalid) { return this.sval.emptyData(this.form) }
    this.spinner.show();
    let body = { ...this.form.value }
    body.empleadoids = this.idsEmployee;
    body.permisos = [];
    this.formList.forEach((e)=>{
      body.permisos.push({
        idpermiso: this.userdata ? e.idformulariopermiso : 0,
        idformulario: e.idformulario,
        estado: e.estado
      });
    })
    this.user.setUser(body).subscribe((response)=>{
      this.spinner.hide();
      this.closeForm.emit(false);
      this.responseok.emit(true);
    }, (e)=> { this.spinner.hide(); this.fn.except(e); });
  }
}
