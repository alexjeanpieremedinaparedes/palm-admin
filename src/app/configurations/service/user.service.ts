import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Envsystem } from 'src/app/core/model/env.model';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { IUser } from '../interface/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private env = new Envsystem();
  private file : string;
  constructor
  (
    private http: HttpClient,
    private fn: FunctionsService
  )
  
  {
    this.file = 'usuario/';
  }

  public getUser() {
    return this.http.get<IUser>(`${this.env.mainUrl}${this.file}listarusuario`,{ headers: this.fn.heardToken() }).pipe( map( (result: any) => result.result ));
  }
  public getForm() {
    return this.http.get<IUser>(`${this.env.mainUrl}${this.file}listarformulario`,{ headers: this.fn.heardToken() }).pipe( map( (result: any) => result.result ));
  }
  public setUser(body:any) {
    return this.http.post(`${this.env.mainUrl}${this.file}agregarusuario`,body ,{ headers: this.fn.heardToken() });
  }
  public deleteUser(body:any) {
    return this.http.post(`${this.env.mainUrl}${this.file}eliminarusuario`,body ,{ headers: this.fn.heardToken() });
  }
}
