import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Envsystem } from 'src/app/core/model/env.model';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { IPassword } from '../interface/seting.interface';

@Injectable({
  providedIn: 'root',
})
export class SettingService {
  
  private env = new Envsystem();
  private file: string;
  constructor(private http: HttpClient, private sfn: FunctionsService) {
    this.file = 'atributo/';
  }
  
  public savePassword(body: any) {
    const headers = this.sfn.heardToken();
    return this.http.post<IPassword>(`${this.env.mainUrl}${this.file}agregarconfiguracion`, body, { headers });
  }
}
