import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoreOptionsComponent } from './pages/more-options/more-options.component';
import { ListUsuarioComponent } from './pages/user/list-user/list-usuario.component';
import { NewUsuarioComponent } from './pages/user/new-user/new-usuario.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { MaterialModule } from '../material-module';
import { ConfiguracionesRoutingModule } from './configuraciones-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    MoreOptionsComponent,
    ListUsuarioComponent,
    NewUsuarioComponent,
  ],
  imports: [
    CommonModule,
    // svg-icon
    HttpClientModule,
    AngularSvgIconModule.forRoot(),
    MaterialModule,
    ConfiguracionesRoutingModule,
    ReactiveFormsModule,
    FormsModule

  ]
})
export class ConfiguracionesModule { }
