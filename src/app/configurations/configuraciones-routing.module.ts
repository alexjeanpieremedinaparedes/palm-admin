import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardComponent } from '../layouts/dashboard/dashboard.component';
import { ListUsuarioComponent } from './pages/user/list-user/list-usuario.component';
import { MoreOptionsComponent } from './pages/more-options/more-options.component';


const routes: Routes = [

  {
    path: '',
    component:  DashboardComponent,
     children : [
       {path: 'usuario', component : ListUsuarioComponent},
       {path: 'masopciones', component : MoreOptionsComponent},
       {path: '**', redirectTo: 'usuario'}
     ]

  },



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracionesRoutingModule {}
