export interface IPassword {
    status:  number;
    message: string;
    result:  IPassResult;
}

export interface IPassResult {
    idconfiguracion: number;
    configuracion:   string;
    valor:           string;
}