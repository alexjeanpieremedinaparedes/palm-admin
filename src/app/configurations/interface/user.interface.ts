
export interface IUser {
  status:  number;
  message: string;
  result:  IUserResult[];
}

export interface IUserResult {
  idusuario: number;
  usuario:   string;
  empleados: IUserEmployee[];
  permisos:  any[];
  token:     string;
}

export interface IUserEmployee {
  numerodocumento: string;
  nombre:          string;
  apellidos:       string;
  nombrecompleto:  string;
  fechanacimiento: string;
  sexo:            boolean;
  direccion:       string;
  telefono:        string;
  correo:          string;
}
