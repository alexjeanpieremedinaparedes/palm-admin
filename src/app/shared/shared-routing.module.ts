import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SiderbarComponent } from './siderbar/siderbar.component';

const routes: Routes = [
  { path: 'siderbar', component: SiderbarComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedRoutingModule { }
