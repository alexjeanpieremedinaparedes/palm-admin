
import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatSidenav } from '@angular/material/sidenav';

import { FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/service/auth.service';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { SettingService } from 'src/app/core/service/setting.service';
import { IAuthResult } from 'src/app/auth/interface/auth.interface';
import { Envsystem } from 'src/app/core/model/env.model';

export interface NavItem {
  displayName: string;
  disabled?: boolean;
  iconName: string;
  estado?: boolean;
  route?: string;
  children?: NavItem[];
}
@Component({
  selector: 'app-siderbar',
  templateUrl: './siderbar.component.html',
  styleUrls: ['./siderbar.component.css']
})
export class SiderbarComponent implements OnInit {

  public dataLogin!: IAuthResult;
  public menutoogle?: boolean;
  public loading?: boolean;
  public isAuthenticated?: boolean;
  public title?: string;
  public isBypass?: boolean;
  public mobile?: boolean;
  public isMenuInitOpen?: boolean;
  public isMenuOpen: boolean = true;
  public contentMargin: number = 240;
  private sidenav?: MatSidenav;
  private env = new Envsystem();
  public search = new FormControl('');

  constructor(
    public sfn: FunctionsService,
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private sauth: AuthService,
    private ssetting: SettingService,
  ) {
    this.loadData();
  }

  ngOnInit() {
    this.isMenuOpen = true; // Open side menu by default
    this.title = 'FactuTED';
    this.search.valueChanges.subscribe( value => {
      const body = {
        url: this.router.url,
        value: value
      };
      this.ssetting.textSearch$.emit(body); 
    });
  }


  private loadData() {
    const encrypt = localStorage.getItem(this.env.dataLogin) ?? '';
    const decrypt = this.sfn.decrypt(encrypt);
    this.dataLogin = decrypt;
    this.dataLogin.logo = this.sfn.prepareBase64ImgForHtml(this.dataLogin.logo);
  }

  public async logout() {

    this.menutoogle = false;
    const head1 = 'Desea';
    const head2 = 'cerrar';
    const head3 = 'Sesion';
    const message = 'Se cerrara la sesion del usuario';
    const contine = await this.sfn.modalQuestion(head1, head2, head3, message, true);
    if(contine) {
      this.sauth.logout();
    }
  }

  get isHandset(): boolean {
    return this.breakpointObserver.isMatched(Breakpoints.Handset);
  }

  // *********************************************************************************************
  // * LIFE CYCLE EVENT FUNCTIONS
  // *********************************************************************************************

  ngDoCheck() {
    if (this.isHandset) {
      this.isMenuOpen = false;
    } else {
      this.isMenuOpen = true;
    }
  }

  // *********************************************************************************************
  // * COMPONENT FUNCTIONS
  // *********************************************************************************************

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  setStep(item: NavItem) {
    this.menu.forEach(el => el.estado = false);
    item.estado = true;
  }

  // *********************************************************************************************
  // * OBJECT MENU
  // *********************************************************************************************

  menu: NavItem[] = [

    // {
    //   displayName: 'Reservas',
    //   iconName: 'assets/svg/reservas.svg',
    //   route: 'entradasGADE',
    // },

    {
      displayName: 'Habitaciones',
      iconName: 'assets/svg/habitaciones.svg',
      route: '/habitaciones/habitaciones',
    },
   
    {
      displayName: 'Personal Operativo',
      iconName: 'assets/svg/personalo.svg',
      route: '/personal/personal',
    },

    // {
    //   displayName: 'Comentarios',
    //   iconName: 'assets/svg/comentarios.svg',
    //   route: 'entradasGADE',
    // },

    // {
    //   displayName: 'Clientes',
    //   iconName: 'assets/svg/clientes.svg',
    //   route: 'entradasGADE',
    // },

    {
      displayName: 'Mi Página',
      iconName: 'assets/svg/mip.svg',
      route: '/mipagina/pagebedrooms',
    },

    // {
    //   displayName: 'Reportes',
    //   iconName: 'assets/svg/reports.svg',
    //   route: 'entradasGADE',
    // },

    {
      displayName: 'Configuraciones',
      iconName: 'assets/svg/config.svg',
      route: '/configuraciones',
      estado: true,
      children: [
       
        {
          displayName: 'Usuarios',
          iconName: 'assets/svg/submenu.svg',
          route: '/configuraciones/usuario',
        },
      
        // {
        //   displayName: 'Más Opciones',
        //   iconName: 'assets/svg/submenu.svg',
        //   route: '/configuraciones/masopciones',

        // },
      ]
    },
  ];

}
