import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './auth/login/login.component';
import { AuthGuard, AuthGuardLogin } from './auth/guards/auth.guard';



const routes: Routes = [

  {
    path: 'login',
    component: LoginComponent,
    canActivate: [AuthGuardLogin]
  },

  {
    path: 'core',
    loadChildren:  () => import ('./core/core.module').then (r => r.CoreModule)
  },

  {
    path: 'shared',
    loadChildren:  () => import ('./shared/shared.module').then (r => r.SharedModule)
  },

  {
    path: 'personal',
    loadChildren:  () => import ('./operational-staff/operational-staff.module').then (r => r.OperationalStaffModule),
    canActivate: [AuthGuard]
  },

  {
    path: 'habitaciones',
    loadChildren:  () => import ('./bedrooms/bedrooms.module').then (r => r.BedroomsModule),
    canActivate: [AuthGuard]
  },

  {
    path: 'mipagina',
    loadChildren:  () => import ('./my-page/my-page.module').then (r => r.MyPageModule),
  
  },

  {
    path: 'configuraciones',
    loadChildren:  () => import ('./configurations/configuraciones.module').then (r => r.ConfiguracionesModule),
    canActivate: [AuthGuard]
  },

  { path: '**', redirectTo: 'login', pathMatch: 'full'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash:true} )],
  exports: [RouterModule]
})
export class AppRoutingModule {}
